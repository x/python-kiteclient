This project is no longer maintained.

The contents of this repository are still available in the Git
source code management system.  To see the contents of this
repository before it reached its end of life, please check out the
following commit with
"git checkout dee7b5b07d8837f08990ce06119f7c5c3b93b7d7"

http://lists.openstack.org/pipermail/openstack-dev/2016-March/090409.html
